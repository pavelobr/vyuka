# Úvod do HTML

## Co je HTML

* HTML (Hyper Text Markup Language) je standartizovaný jazyk, který popisuje dokument webové stránky
(HTML dokument).
* HTML popisuje strukturu dokumentu webové stránky.
* Struktura je dána značkami (dále tagy). Tagy určují jednotlivé elementy a z elementů je pak tvořena
  struktura HTML dokumentu. Elementem jsou jednotlivé části dokumentu jako nadpisy, odstavce, odrážky,
  vložené obrázky a další elementy, které známe z webových stránek.
* Webové prohlížeče (Chrome, Firefox, atd.) využívají HTML dokument k vykreslení webové stránky. Pro
  webové prohlížeče je HTML popisem toho, jak mají webovou stránku zobrazit.

## W3C

HTML je standart, který vytváří organizace s názvem World Wide Web Consorcium (www.w3c.org). Cílem
této organizace je vytváření a udržovaní tzv. **Open Web Platform**, což je vlastně snaha o standartizaci
mnoha různých technologií zajišťujících funkčnost webu.

Členy organizace jsou významné firmy, univerzity a další organizace, pro které je web a jeho technologie významnou částí jejich činnosti.
Mezi členy jsou například firmy jako Google, Microsoft, Apple, Akamai Technologies, Clodflare a mnoho dalších
(viz. [seznam aktuálních členů](https://www.w3.org/Consortium/Member/List))

## Minimální HTML dokument

Pokud chceme vytvořit alespoň minimální HTML dokument, měli bychom dodržet tuto minimální strukturu:

```html
<!DOCTYPE html>
<html>
    <head>
        <title>Vítej!</title>
        <meta charset="utf-8">
    </head>
    <body>
        <h1>Vítej!</h1>
        <p>Právě jsi navštívil mojí první webovou stránku</p>
    </body>
</html>
```

## Tagy

Jak již bylo uvedeno, struktura dokumentu je tvořena jednotlivými elementy, které jsou určeny značkami (tagy).
Tagy jsou **párové** a **nepárové**.

### Párový tag

Párový tak je takový, kde začátek elementu je tvořen počátečním tagem a konec elementu je určen ukončujícím tagem.
V našem minimálním HTML dokumentu jde například o případ tagu ```<h1>```. Je tak určujen začátek a konec textu,
který tvoří nadpis:

```html
<h1>Vítej</h1>
```

### Nepárový tag

Nepárový tak je takový tag, který neurčuje začátek nebo konec nějakého elementu, ale obvykle uvádí doplňující
informace v dokumentu. V případě našeho minimálního HTML dokumentu se jedná naříklad o tag ```<meta>```.

Tag ```<meta charset=utf-8>``` určuje v jaké znakové sadě je dokument vytvořen. Uvedením tohoto tagu zajistíme
správnou interpretaci interpunkce a dalšíh "zvláštních" znaků našeho jazyka.

## Vysvětlení struktury minimálního HTML dokumentu

* ```<!DOCTYPE html>``` určuje, že se jedná o dokument typu HTML. (Jsou i jiné typy dokumentů, například XML)
* ```<html>``` je kořenový element dokumentu, který obaluje veškerý html kód dokumentu
* ```<head>``` obsahuje popisné (tzv. metainformace) o dokumentu
* ```<title>``` definuje titulek stránky. Titulek je vidět v záhlaví okna prohlížeče.
* ```<meta charset="utf-8">``` určuje v jaké znakové sadě je dokument vytvořen
* ```<body>``` určuje viditelnou část dokumentu.
* ```<h1>``` definuje nadpis dokumentu. Konkrétně se jedná o první úroveň nadpisu (máme další - nižší úrovně).
* ```<p>``` definuje odstavec textu.
