# Editace dat v tabulce

## UPDATE, SET

Pro procvičení použijte dump databáze pujcovna [04-pujcovna.sql](04-pujcovna.sql).
Editaci záznamů provádíme kombinací příkazů `UPDATE` a `SET`.
Mějme tabulku `vypujcky` v databázi `pujcovna`:

```sql
MariaDB [pujcovna]> SELECT * FROM vypujcky;
+------+------------+------------+-----------+------------+-----------------+---------------+
| id   | jmeno      | prijmeni   | predmet   | cena_s_dph | datum_vypujceni | datum_vraceni |
+------+------------+------------+-----------+------------+-----------------+---------------+
|    1 | Jeník      | Vobořil    | kajak     |     300.00 | 2017-06-10      | 2017-12-19    |
|    2 | Karel      | Stráský    | spacák    |     150.00 | 2017-04-13      | 2017-12-19    |
|    3 |  Emil      | Novák      | lyže      |     180.00 | 2017-01-22      | 2017-12-19    |
|    4 |   Jana     | Nováková   | lyže      |     180.00 | 2017-03-10      | 2017-12-19    |
```

## Nahrazení hodnot jednou hodnotou pro celý sloupec

Změna obsahu sloupce `predmet` v celé tabulce a nahrazení všech hodnot jinou hodnotou provedeme příkazem `UPDATE vypujcky SET predmet="pádlo";`:

```sql
MariaDB [pujcovna]> UPDATE vypujcky SET predmet="pádlo";
Query OK, 1 row affected (0.02 sec)
Rows matched: 1  Changed: 1  Warnings: 0

MariaDB [pujcovna]> SELECT * FROM vypujcky;
+------+------------+------------+-----------+------------+-----------------+---------------+
| id   | jmeno      | prijmeni   | predmet   | cena_s_dph | datum_vypujceni | datum_vraceni |
+------+------------+------------+-----------+------------+-----------------+---------------+
|    1 | Jeník      | Vobořil    | pádlo     |     300.00 | 2017-06-10      | 2017-12-19    |
|    2 | Karel      | Stráský    | pádlo     |     150.00 | 2017-04-13      | 2017-12-19    |
|    3 |  Emil      | Novák      | pádlo     |     180.00 | 2017-01-22      | 2017-12-19    |
|    4 |   Jana     | Nováková   | pádlo     |     180.00 | 2017-03-10      | 2017-12-19    |
```

Všimněte si, že došlo k nastavení hodnoty `pádlo` v celém sloupci `predmet`!

## Editace konkrétního záznamu v tabulce (filtr WHERE)

Pokud chceme nastavit jinou hodnotu prouze pro konkrétní záznam (řádek v tabulce), můžeme použít tzv. filtr. Filtrování provedeme prostřednictvím klíčového slova `WHERE`.
Pokud chceme změnit hodnotu ve sloupci `predmet` pouze pro zákazníka `Karel Stránský` s `id=2`, zkombinujeme příkaz `UPDATE` s filtrem `WHERE`:

```sql
MariaDB [pujcovna]> UPDATE vypujcky SET predmet="vesta" WHERE id=2;
Query OK, 1 row affected (0.02 sec)
Rows matched: 1  Changed: 1  Warnings: 0

MariaDB [pujcovna]> SELECT * FROM vypujcky;
+------+------------+------------+-----------+------------+-----------------+---------------+
| id   | jmeno      | prijmeni   | predmet   | cena_s_dph | datum_vypujceni | datum_vraceni |
+------+------------+------------+-----------+------------+-----------------+---------------+
|    1 | Jeník      | Vobořil    | pádlo     |     300.00 | 2017-06-10      | 2017-12-19    |
|    2 | Karel      | Stráský    | vesta     |     150.00 | 2017-04-13      | 2017-12-19    |
|    3 |  Emil      | Novák      | pádlo     |     180.00 | 2017-01-22      | 2017-12-19    |
|    4 |   Jana     | Nováková   | pádlo     |     180.00 | 2017-03-10      | 2017-12-19    |
```

Samozřejmě můžeme změnit i více hodnot v jednom záznamu. Zde konkrétně změníme pro záznam s `id=3` hodnotu sloupce `predmet` a `prijmeni`. Všimněte si, že jsou jednotlivé sloupce odděleny čárkou `,`:

```sql
MariaDB [pujcovna]> UPDATE vypujcky SET predmet="vesta", prijmeni ="Novotný" WHERE id=3;
Query OK, 1 row affected (0.01 sec)
Rows matched: 1  Changed: 1  Warnings: 0

MariaDB [pujcovna]> SELECT * FROM vypujcky;
+------+------------+------------+-----------+------------+-----------------+---------------+
| id   | jmeno      | prijmeni   | predmet   | cena_s_dph | datum_vypujceni | datum_vraceni |
+------+------------+------------+-----------+------------+-----------------+---------------+
|    1 | Jeník      | Vobořil    | kajak     |     300.00 | 2017-06-10      | 2017-12-19    |
|    2 | Karel      | Stráský    | vesta     |     150.00 | 2017-04-13      | 2017-12-19    |
|    3 |  Emil      | Novotný    | vesta     |     180.00 | 2017-01-22      | 2017-12-19    |
|    4 |   Jana     | Nováková   | lyže      |     180.00 | 2017-03-10      | 2017-12-19    |
```

Trochu předběhneme ve výkladu, ale pomocí boolenovské algebry lze změnit hodnoty ve více řádcích najednou. Nastavení hodnoty sloupce `predmet` na hodnotu `lano` pro dva záznamy (`id=1` a `id=4`):

```sql
MariaDB [pujcovna]> UPDATE vypujcky SET predmet="lano" WHERE id=1 OR id=4;
Query OK, 2 rows affected (0.01 sec)
Rows matched: 2  Changed: 2  Warnings: 0

MariaDB [pujcovna]> SELECT * FROM vypujcky;
+------+------------+------------+-----------+------------+-----------------+---------------+
| id   | jmeno      | prijmeni   | predmet   | cena_s_dph | datum_vypujceni | datum_vraceni |
+------+------------+------------+-----------+------------+-----------------+---------------+
|    1 | Jeník      | Vobořil    | lano      |     300.00 | 2017-06-10      | 2017-12-19    |
|    2 | Karel      | Stráský    | vesta     |     150.00 | 2017-04-13      | 2017-12-19    |
|    3 |  Emil      | Novotný    | vesta     |     180.00 | 2017-01-22      | 2017-12-19    |
|    4 |   Jana     | Nováková   | lano      |     180.00 | 2017-03-10      | 2017-12-19    |
```

## Odstranení konkrétního záznamu z tabulky (příkaz DELETE)

Příkaz `DELETE` odstraňuje záznamy, tedy řádky v tabulce. Obvykle chceme smazat jeden nebo více záznamů. Pro určení konkrétních záznamů využíváme filtr `WHERE`.
Můžeme smazat konkrétní záznam, například záznam s `id=1`:

```sql

MariaDB [pujcovna]> DELETE FROM vypujcky WHERE id=1;
Query OK, 1 row affected (0.01 sec)


MariaDB [pujcovna]> SELECT * FROM vypujcky;
+------+------------+------------+-----------+------------+-----------------+---------------+
| id   | jmeno      | prijmeni   | predmet   | cena_s_dph | datum_vypujceni | datum_vraceni |
+------+------------+-------`-----+-----------+------------+-----------------+---------------+
|    2 | Karel      | Stráský    | vesta     |     150.00 | 2017-04-13      | 2017-12-19    |
|    3 |  Emil      | Novotný    | vesta     |     180.00 | 2017-01-22      | 2017-12-19    |
|    4 |   Jana     | Nováková   | lano      |     180.00 | 2017-03-10      | 2017-12-19    |
```

Lze smazat i více záznamů najednou. Takto odstraníme záznamy s `id=2` a `id=3`:

```sql
MariaDB [pujcovna]> DELETE FROM vypujcky WHERE id=2 OR id=3;
Query OK, 2 rows affected (0.00 sec)

MariaDB [pujcovna]> SELECT * FROM vypujcky;
+------+------------+------------+-----------+------------+-----------------+---------------+
| id   | jmeno      | prijmeni   | predmet   | cena_s_dph | datum_vypujceni | datum_vraceni |
+------+------------+------------+-----------+------------+-----------------+---------------+
|    4 |   Jana     | Nováková   | lano      |     180.00 | 2017-03-10      | 2017-12-19    |
```

## Odstranění všech záznamů z tabulky

Pokud chceme vymazat veškerý obsah tabulky můžeme využít příkaz `DELETE` nebo `TRUNCATE`.

Pomocí příkazu `DELETE`:

```sql
MariaDB [pujcovna]> DELETE FROM vypujcky;
```

Pomocí příkazu `TRUNCATE`:

```sql
MariaDB [pujcovna]> TRUNCATE vypujcky;
```