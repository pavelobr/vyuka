-- MySQL dump 10.16  Distrib 10.1.38-MariaDB, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: pujcovna
-- ------------------------------------------------------
-- Server version	10.1.38-MariaDB-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `pujcovna`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `pujcovna` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `pujcovna`;

--
-- Table structure for table `vypujcky`
--

DROP TABLE IF EXISTS `vypujcky`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vypujcky` (
  `id` smallint(5) unsigned DEFAULT NULL,
  `jmeno` varchar(20) DEFAULT NULL,
  `prijmeni` varchar(20) DEFAULT NULL,
  `predmet` varchar(20) DEFAULT NULL,
  `cena_s_dph` decimal(9,2) DEFAULT NULL,
  `datum_vypujceni` date DEFAULT NULL,
  `datum_vraceni` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vypujcky`
--

LOCK TABLES `vypujcky` WRITE;
/*!40000 ALTER TABLE `vypujcky` DISABLE KEYS */;
INSERT INTO `vypujcky` VALUES (1,'Jeník','Vobořil','kajak',300.00,'2017-06-10','2017-12-19'),(2,'Karel','Stráský','spacák',150.00,'2017-04-13','2017-12-19'),(3,' Emil','Novák','lyže',180.00,'2017-01-22','2017-12-19'),(4,'  Jana  ','Nováková','lyže',180.00,'2017-03-10','2017-12-19'),(5,'Eva','Krátká','spacák',150.00,'2017-05-25','2017-12-19'),(6,'Pepa','Veselý','stan',200.00,'2017-01-12','2017-12-19'),(7,'Kája ','Lopuch','kolo',310.00,'2017-03-03','2017-12-19'),(8,'  Ladislav','Pepřek','kajak',300.00,'2017-02-02','2017-12-19'),(9,'Jarmila','Krásná','kolo',310.00,'2017-07-21','2017-12-19'),(10,' Jarmila','Krásná','karimatka',50.00,'2017-06-06','2017-12-19'),(11,'Jan','Veselý','spacák',150.00,'2017-04-10','2017-12-19'),(12,'Karel','Neveselý','lyže',180.00,'2017-03-16','2017-12-19'),(13,'  Jana  ','Nováková','kolo',310.00,'2017-01-12','2017-12-19'),(14,'Eva','Krátká','kajak',300.00,'2017-03-03','2017-12-19'),(15,'Pepa','Veselý','kolo',310.00,'2017-02-02','2017-12-19'),(16,'Kája ','Lopuch','karimatka',50.00,'2017-07-21','2017-12-19'),(17,'  Ladislav','Pepřek','spacák',150.00,'2017-06-06','2017-12-19'),(18,'Jarmila','Krásná','lyže',180.00,'2017-04-10','2017-12-19'),(19,' Jarmila','Krásná','kolo',310.00,'2017-03-16','2017-12-19'),(20,'Jan','Veselý','karimatka',50.00,'2017-04-10','2017-12-19');
/*!40000 ALTER TABLE `vypujcky` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-02-11 18:46:46
