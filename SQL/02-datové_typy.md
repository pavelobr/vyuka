# Datové typy

## Jak to je s datvými typy v různých DBMS

Už máte za sebou zkušenost s programováním z jiného předmětu ve škole a tak vám termín datový typ je jistě zřejmý. Jak víte, tak v relačních databázích ukládáte data do tabulek. Každý sloupec tabulky musí mít určený datový typ. Datový typ vám umožní zajistit, že v tabulce budou data s hodnotami, které očekáváte. Tedy pokud bude řečeno, že ve sloupci tabulky lze ukládat pouze celá čísla, nemůžete do zmíněného sloupce vložit text "Karel nese čaj".

Různé databázové systémy se od sebe liší i různými datovými typy. Obvykle DBMS umožňují pracovat s těmito datovými typy:

* číselné
* textové
* pravdivostní

Konkrétně pro MariaDB jde o tyto datvé typy:

* TINYINT
* BOOLEAN - Synonym for TINYINT(1)
* SMALLINT
* MEDIUMINT
* INT, INTEGER
* BIGINT
* DECIMAL, DEC, NUMERIC, FIXED
* FLOAT
* DOUBLE, DOUBLE PRECISION, REAL
* BIT

* CHAR
* VARCHAR
* BINARY
* CHAR BYTE
* TINYBLOB
* BLOB
* MEDIUMBLOB
* LONGBLOB
* TINYTEXT
* TEXT
* MEDIUMTEXT
* LONGTEXT
* JSON Data Type
* ENUM
* SET

* DATE
* TIME
* DATETIME
* TIMESTAMP
* YEAR

* ...a další datové typy :)

Jak vidíte, je jich hodně a není naším cílem je všechny znát zpaměti. Pro tyto účely máme dokumentaci.
> Zde: https://mariadb.com/kb/en/library/data-types/ naleznete kompletní popis a výpis podporovaných datových typů v MariaDB.

## Vliv volby datového typu na práci s databází

### CHAR versus VARCHAR

Volba datového typu ovlivňuje významně chování DBMS. Jako příklad vezměme datový typ `CHAR` a `VARCHAR`. Oba dva datové typy jsou určeny pro ukládání textových řetězců. `CHAR` umožňuje uložit text o maximální délce 255 znaků a `VARCHAR` text o maximální délce 65 535 znaků.
Obvykle potřebujeme ukládat krátké textové hodnoty například tvaru jmen ("Pavel", "Jana", "Pepa"). Mohu tedy vytvořit tabulku `lidi_1`, která bude vést jména a příjmení lidí a bude využívat datový typ `CHAR`, a tabulku `lidi_2`, která bude vést ta samá data, ale bude využívat poze datový typ `VARCHAR`.

```sql
CREATE TABLE lidi_1(jmeno CHAR(20), prijmeni CHAR(20))
CREATE TABLE lidi_2(jmeno VARCHAR(20), prijmeni VARCHAR(20))
```

Číslo 20 v závorce za datovým typem určuje, že:

* Nemůžeme uložit informaci delší než 20 znaků.
* V případě datového typu `CHAR` rezervujeme právě 20 znaků pro uložení hodnoty. Znamená to tedy, že pokud budu ukládat hodnotu "Jana" o délce čtyř znaků, bude délka uložené informace nastavených 20 znaků.
* V případě datového typu `VARCHAR` rezervujeme maximálně 20 znaků pro uložení hodnoty. Znamená to tedy, že pokud budu ukládat hodnotu "Jana" o délce čtyř znaků, bude délka uložené informace 4 znaky.

Co to znamená pro chování databázového systému? Na první pohled je jasné, že pokud budeme používat datový typ `VARCHAR`, bude objem dat menší.
Proč tedy volit `CHAR`? `CHAR` má naopak jinou výhodu a to je rychlost hledání v databázi. V případě volby datového typu `CHAR` jsou výsledky hledání výrazně rychlejší (viz. https://stackoverflow.com/questions/1885630/whats-the-difference-between-varchar-and-char).
Jak se tedy rozhodnout? To je na vaší aplikaci. Pokud je významnější kritérium rychlost využijete `CHAR`, pokud upřednostňujete nízké obsazení diskového prostoru, využijete `VARCHAR`.

### FLOAT, DOUBLE versus DECIMAL

`FLOAT`, `DOUBLE` a `DECIMAL` jsou datové typy, které slouží pro ukládání čísel s desetinnou čárkou. Všechny tyto datové typy se deklarují ve tvaru `datový_typ(velikost, přesnost)`. Datový typ může být `FLOAT`, `DOUBLE` nebo `DECIMAL`. **Velikost** reprezentuje délku řetězce (bez desetinné čárky) a **přesnost** vyjadřuje počet desetinných míst.

Takto vytvoříme tabulku, která obsahuje např. délku pozemku:

```sql
CREATE TABLE pozemek(nazev VARCHAR(50), delka DECIMAL(10,2))
```

`DECIMAL(10,2)` znamená, že můžeme uložit maximálně číslo o hodnotě 99999999,99:

```sql
# Všimněte si, že desetinnou čárku vyjadřujeme znakem "." (tečka)
INSERT INTO pozemek(nazev, delka) VALUES("u hřbitova", 99999999.99)
```

Kdy použít `FLOAT`, `DOUBLE` nebo `DECIMAL`? `DECIMAL` použijte v případě, že potřebujete uložit pevně dané **konečné** číslo - například `100.50`. Datové typy `FLOAT` nebo `DOUBLE` jsou určeny pro uložení "nepřesných" desetinných čísel, použijte je v případě, že potřebujete uložit hodnotu, která například vznikne výpočtem "deset děleno třemi". Pro naši výuku zatím vystačme s tím, že se takovým datovým datovým typům budeme vyhýbat, protože v našich příkladech budeme ukládat ceny, koečné počty něčeho, atd..
> [Zde](https://www.intechgrity.com/mysql-datatypes-working-with-fraction-and-decimal-dec/#), pro čtivé, je pojednání o rozdílech.