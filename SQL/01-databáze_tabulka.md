# Vytvoření databáze a tabulky

## Databáze

Databáze je vlastně jakýsi kontejner ve kterém jsou uložené a organizované tabulky. Databáze může obsahovat žádnou tabulku (pak ani nemáme v databázi žádná data) nebo jednu či typicky více tabulek.

## Příkazy pro práci s databází

### Konvence

Příkazy SQL by se měly psát velkým písmem. Názvy tabulek a polí je rozumnější vytvářet a psát s malým písmem, výsledný SQL kód je pak přehlednější.

```sql
INSERT INTO moje_tabulka(id INT, nazev VARCHAR(20));
```

### Znak ";"

Znak `;` slouží jako oddělovač příkazů. Je to tzv. delimiter. Napíšete-li `;` a zmáčknete-li `[Enter]`, databázový software pak považuje vše, co je uvedené před znakem `;` za příkaz a začne jej provádět.

Tj. takto se příkaz provede po stisknutí `[Enter]`:

```sql
SELECT * FROM trida;
```

Takto se po stisknutí `[Enter]` příkaz neprovede (místo toho dojde k odřádkování na další řádek):

```sql
SELECT * from trida
```

### Vytvoření databáze

Příkaz `CREATE DATABASE` založí databázi. Založíme tedy databázi s názvem `trida`.

```sql
CREATE DATABASE trida;
```

Příkaz `SHOW CREATE DATABASE trida;` zobrazí, jak byla databáze vytvořena.

```sql
MariaDB [trida]> SHOW CREATE DATABASE trida;
+----------+-------------------------------------------------------------------+
| Database | Create Database                                                   |
+----------+-------------------------------------------------------------------+
| trida    | CREATE DATABASE `trida` /*!40100 DEFAULT CHARACTER SET utf8mb4 */ |
+----------+-------------------------------------------------------------------+
```

### Zobrazení existujících databází

Jak jsme již uvedli, tak je obvyklé, že databázový software spravuje více než jednu databázi. Pokud je chceme zobrazit, použijeme příkaz `SHOW DATABASES`.

```sql
SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| lidi               |
| mysql              |
| performance_schema |
| trida              |
+--------------------+
```

### Označení aktivní databáze

Typické je, že databázový software spravuje více jednotlivých databází. Pokud vím, že chci pracovat s databází `trida`, tak použiju příkaz `USE`.

```sql
USE trida;
```

Když jsem označil aktivní databázi, mohu začit v databázi pracovat. Mohu tedy i vytvářet nové tabulky a vkládat do nich data.

### Vytvoření tabulky

Tabulka je dvourozměrný objekt s řádky a sloupci. Ve sloupcíh jsou uvedeny názvy jednotlivých polí, samotná data jsou poté zaznamenány v řádcích tabulky, řádek s daty označujeme jako **záznam**.
Nejprve je nutné vytvořit strukturu tabulky. Později si vše vysvětlíme podrobněji, ale nyní vytvořme tabulku příkazem `CREATE TABLE`.

Vytvoření tabulky s názvem `zaci`, která obsahuje pole s názvy `id`, `jmeno` a `prijmeni` (`INT` a `VARCHAR` jsou datové typy polí - později vysvětlíme).

```sql
CREATE TABLE zaci(id INT, jmeno VARCHAR(20), prijmeni VARCHAR(20));
```

Strukturu vytvořené tabulky lze zobrazit příkazem `DESCRIBE`:

```sql
MariaDB [trida]> DESCRIBE zaci;
+----------+-------------+------+-----+---------+-------+
| Field    | Type        | Null | Key | Default | Extra |
+----------+-------------+------+-----+---------+-------+
| id       | int(11)     | YES  |     | NULL    |       |
| jmeno    | varchar(20) | YES  |     | NULL    |       |
| prijmeni | varchar(20) | YES  |     | NULL    |       |
+----------+-------------+------+-----+---------+-------+
```

Příkaz `SHOW CREATE TABLE zaci` zobrazí jakým příkazem je tabulka `zaci` vytvořena. Vyzkoušejte.

### Zobrazení tabulek v databázi

Většina databází obsahuje více než jednu tabulku. Pro jejich zobrazení použijme příkaz `SHOW TABLES`. V naší databázi `trida` je tedy pouze jedna tabulka `zaci`.

```sql
MariaDB [trida]> SHOW TABLES;
+-----------------+
| Tables_in_trida |
+-----------------+
| zaci            |
+-----------------+
```

### Vložení dat do tabulky

Příkaz `INSERT INTO` vloží do tabulky data. Takto naplníme tabulku `trida`:

```sql
INSERT INTO zaci(id, jmeno, prijmeni) VALUES(1, "Petra", "Nováková");
```

Vložení více záznamů najednou:

```sql
INSERT INTO zaci(id, jmeno, prijmeni) VALUES(2, "Petr", "Novák"),(3, "Jan", "Veselý");
```

### Zobrazení dat v tabulce

Trochu předběhneme, ale příkazem `SELECT` zobrazíme všechna data z tabulky `zaci`:

```sql
MariaDB [trida]> SELECT * FROM zaci;
+------+-------+------------+
| id   | jmeno | prijmeni   |
+------+-------+------------+
|    1 | Petra | Nováková   |
|    2 | Petr  | Novák      |
|    3 | Jan   | Veselý     |
+------+-------+------------+
3 rows in set (0.00 sec)
```

### Odstranění tabulky a databáze

Příkaz `DROP` maže objekty v databázi. Nemaže data, ale celé struktury obsahující data.
Smazání celé tabulky provedeme příkazem `DROP TABLE`. Tím jsme samozřejmě přijdeme i o veškerá data v tabulce!

Takto smažeme tabulku `zaci`:

```sql
DROP TABLE zaci;
```

Databázi i se všemi tabulkami a daty v tabulkách odstraníme také příkazem `DROP`. Příkazem `DROP DATABASE` odstraní nevratně celou databázi.

Smazání databáze `trida`:

```sql
DROP DATABASE trida;
```
