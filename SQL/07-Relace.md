# Relace v databázi (dvě tabulky)
## Příklad: Tabulka knih a tabulka autorů

### 1. Úvod
V relačních databázích, jsou data organizována do tabulek, které mohou mít mezi sebou různé typy relací. Relace mezi tabulkami umožňují propojit data a umožňují efektivní dotazování a analýzu informací. Relace se často používají k definování vztahů mezi tabulkami, což může zahrnovat jedinečné identifikátory a klíče.

V tomto dokumentu se zaměříme na příklad dvou tabulek: **knihy** a **autoři**, a ukážeme, jak mezi nimi vytvořit relace.

### 2. Struktura databáze
Vytvoříme dvě tabulky:
- **Tabulka autorů** (`autori`) – obsahuje informace o autorech knih.
- **Tabulka knih** (`knihy`) – obsahuje informace o knihách, přičemž každá kniha je přiřazena jednomu autorovi.

### 3. Vztah mezi tabulkami
Vztah mezi tabulkami je definován pomocí cizího klíče (foreign key). V tomto příkladu bude mít tabulka `knihy` cizí klíč, který odkazuje na primární klíč tabulky `autori`. Tento cizí klíč ukazuje, který autor napsal konkrétní knihu.

### 4. Vytvoření tabulek

#### 4.1. Tabulka autorů (`autori`)

Tato tabulka bude obsahovat informace o autorech: jejich unikátní ID, jméno, příjmení a rok narození.

```sql
CREATE TABLE autori (
    id_autora INT AUTO_INCREMENT PRIMARY KEY,  -- Primární klíč, automaticky se zvyšuje
    jmeno VARCHAR(100) NOT NULL,               -- Jméno autora
    prijmeni VARCHAR(100) NOT NULL,            -- Příjmení autora
    rok_narozeni INT                           -- Rok narození autora
);
```

- `id_autora` je primární klíč tabulky a zajišťuje unikátnost každého autora.
- `jmeno` a `prijmeni` jsou povinné sloupce pro jméno a příjmení autora.
- `rok_narozeni` je volitelný sloupec pro rok narození.

#### 4.2. Tabulka knih (`knihy`)

Tato tabulka bude obsahovat informace o knihách: název knihy, rok vydání, žánr a cizí klíč odkazující na autora.

```sql
CREATE TABLE knihy (
    id_knihy INT AUTO_INCREMENT PRIMARY KEY,   -- Primární klíč pro knihu
    nazev VARCHAR(255) NOT NULL,               -- Název knihy
    rok_vydani INT,                            -- Rok vydání
    zanr VARCHAR(100),                         -- Žánr knihy
    id_autora INT,                             -- Cizí klíč na tabulku autorů
    FOREIGN KEY (id_autora) REFERENCES autori(id_autora)  -- Relace s tabulkou autori
);
```

- `id_knihy` je primární klíč tabulky knih.
- `nazev` je název knihy, který je povinný.
- `rok_vydani` je rok vydání knihy.
- `zanr` je žánr knihy.
- `id_autora` je cizí klíč, který ukazuje na autora knihy v tabulce `autori`.

### 5. Příklady použití cizího klíče

Relace mezi tabulkami knih a autorů je realizována pomocí cizího klíče `id_autora` v tabulce `knihy`. Tento cizí klíč odkazuje na primární klíč `id_autora` v tabulce `autori`. To znamená, že každá kniha je přiřazena konkrétnímu autorovi.

Příklad:

Pokud máme autora s ID 1 (například autor "J.K. Rowling"), můžeme vytvořit záznam o jeho knize takto:

```sql
-- Vložení autora do tabulky autori
INSERT INTO autori (jmeno, prijmeni, rok_narozeni) 
VALUES ('J.K.', 'Rowling', 1965);

-- Vložení knihy do tabulky knihy
INSERT INTO knihy (nazev, rok_vydani, zanr, id_autora) 
VALUES ('Harry Potter a kámen mudrců', 1997, 'Fantasy', 1);
```

Tímto způsobem propojujeme knihu s autorem pomocí cizího klíče `id_autora`.

### 6. Dotazy na data

S využitím relací mezi tabulkami můžeme vytvářet složité dotazy, které spojí informace o knihách a autorech.

#### 6.1. Výběr všech knih a jejich autorů

Tento dotaz vybere název knihy a jméno autora z obou tabulek pomocí INNER JOIN:

```sql
SELECT knihy.nazev, autori.jmeno, autori.prijmeni
FROM knihy
INNER JOIN autori ON knihy.id_autora = autori.id_autora;
```

Výstup bude vypadat například takto:

| nazev                            | jmeno  | prijmeni |
|----------------------------------|--------|----------|
| Harry Potter a kámen mudrců      | J.K.   | Rowling  |

#### 6.2. Výběr všech knih od určitého autora

Pokud chceme získat všechny knihy od autora s ID 1 (například J.K. Rowling), použijeme tento dotaz:

```sql
SELECT knihy.nazev
FROM knihy
WHERE knihy.id_autora = 1;
```

Tento dotaz vrátí seznam knih napsaných autorem s ID 1.

### 7. Grafické vyjádření relace

Níže je znázornění vztahu mezi tabulkami `autori` a `knihy` pomocí cizího klíče `id_autora`. Tento diagram ukazuje, jak jsou tabulky propojeny.

```plaintext
+-------------------+       +---------------------+
|     autori        |       |      knihy          |
+-------------------+       +---------------------+
| id_autora (PK)    |<----- | id_autora (FK)      |
| jmeno             |       | id_knihy (PK)       |
| prijmeni          |       | nazev               |
| rok_narozeni      |       | rok_vydani          |
+-------------------+       | zanr                |
                            +---------------------+
```

- **PK** označuje **primární klíč** (unikátní identifikátor záznamu v tabulce).
- **FK** označuje **cizí klíč**, který je propojen s primárním klíčem jiné tabulky.

V tomto případě je cizí klíč `id_autora` v tabulce `knihy`, který odkazuje na primární klíč `id_autora` v tabulce `autori`. To znamená, že každá kniha je přiřazena jednomu autorovi.

### 8. Závěr

V tomto dokumentu jsme se seznámili s vytvořením dvou tabulek v databázi MariaDB: `autori` a `knihy`, a jak mezi nimi definovat relaci pomocí cizího klíče. Tato relace umožňuje efektivně propojit informace o autorech a jejich knihách a provádět složité dotazy, které získávají související data z více tabulek.

Relace mezi tabulkami jsou základním principem relačních databází a jejich správné nastavení je klíčové pro konzistentnost a integritu dat.