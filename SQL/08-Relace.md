# Relace v databázi (3 tabulky)
## Návrh databázového modelu s tabulkou čtenářů a výpůjček

Tento dokument rozšiřuje stávající databázový model o tabulku **čtenáři** a tabulku **výpůjčky**, která eviduje vztahy mezi čtenáři a knihami, které si půjčují.

---

#### 1. Stávající tabulky
**Tabulka autorů (`autori`)** – obsahuje informace o autorech knih.
```sql
CREATE TABLE autori (
    id_autora INT AUTO_INCREMENT PRIMARY KEY,  -- Primární klíč
    jmeno VARCHAR(100) NOT NULL,               -- Jméno autora
    prijmeni VARCHAR(100) NOT NULL,            -- Příjmení autora
    rok_narozeni INT                           -- Rok narození autora
);
```

**Tabulka knih (`knihy`)** – obsahuje informace o knihách, přičemž každá kniha je přiřazena jednomu autorovi.
```sql
CREATE TABLE knihy (
    id_knihy INT AUTO_INCREMENT PRIMARY KEY,   -- Primární klíč
    nazev VARCHAR(255) NOT NULL,               -- Název knihy
    rok_vydani INT,                            -- Rok vydání
    zanr VARCHAR(100),                         -- Žánr knihy
    id_autora INT,                             -- Cizí klíč na tabulku autorů
    FOREIGN KEY (id_autora) REFERENCES autori(id_autora)
);
```

---

#### 2. Nové tabulky
**Tabulka čtenáři (`ctenari`)** – eviduje základní informace o čtenářích.
```sql
CREATE TABLE ctenari (
    id_ctenare INT AUTO_INCREMENT PRIMARY KEY,  -- Primární klíč
    jmeno VARCHAR(100) NOT NULL,                -- Jméno čtenáře
    prijmeni VARCHAR(100) NOT NULL,             -- Příjmení čtenáře
    email VARCHAR(255) NOT NULL UNIQUE,         -- E-mail čtenáře (unikátní)
    telefon VARCHAR(20),                        -- Telefon (nepovinný)
    datum_registrace DATE DEFAULT CURRENT_DATE  -- Datum registrace
);
```

**Tabulka výpůjčky (`vypujcky`)** – propojuje čtenáře a knihy a zaznamenává informace o výpůjčkách.
```sql
CREATE TABLE vypujcky (
    id_vypujcky INT AUTO_INCREMENT PRIMARY KEY,  -- Primární klíč
    id_ctenare INT NOT NULL,                     -- Cizí klíč na tabulku čtenáři
    id_knihy INT NOT NULL,                       -- Cizí klíč na tabulku knihy
    datum_vypujceni DATE NOT NULL,               -- Datum vypůjčení knihy
    datum_vraceni DATE,                          -- Datum vrácení knihy (volitelné)
    FOREIGN KEY (id_ctenare) REFERENCES ctenari(id_ctenare),
    FOREIGN KEY (id_knihy) REFERENCES knihy(id_knihy)
);
```

---

#### 3. Grafické vyjádření relací
Níže je znázornění vztahů mezi tabulkami v databázovém modelu.

```plaintext
+-------------------+     +---------------------+       +------------------+
|      autori       |     |       knihy         |       |      ctenari     |
+-------------------+     +---------------------+       +------------------+
| id_autora (PK)    |<----| id_autora (FK)      |   +---| id_ctenare (PK)  |
| jmeno             |   +-| id_knihy (PK)       |   |   | jmeno            |
| prijmeni          |   | | nazev               |   |   | prijmeni         |
| rok_narozeni      |   | | rok_vydani          |   |   | email            |
+-------------------+   | | zanr                |   |   | telefon          |
                        | +---------------------+   |   | datum_registrace |
                        |                           |   +------------------+
                        |                           |
                        |                           |
                      +-----------------------------+
                      | |
+-------------------+ | |
|    vypujcky       | | |
+-------------------+ | |
| id_vypujcky (PK)  | | |
| id_ctenare (FK)   |<+ |
| id_knihy (FK)     |<--+
| datum_vypujceni   |
| datum_vraceni     |
+-------------------+
```

---

#### 4. Příklady SQL dotazů
**1. Vložení nového čtenáře:**
```sql
INSERT INTO ctenari (jmeno, prijmeni, email, telefon)
VALUES ('Jan', 'Novák', 'jan.novak@example.com', '123456789');
```

**2. Vložení nové výpůjčky:**
```sql
INSERT INTO vypujcky (id_ctenare, id_knihy, datum_vypujceni)
VALUES (1, 2, '2024-11-28');
```

**3. Seznam výpůjček konkrétního čtenáře:**
```sql
SELECT ctenari.jmeno, ctenari.prijmeni, knihy.nazev, vypujcky.datum_vypujceni, vypujcky.datum_vraceni
FROM vypujcky
JOIN ctenari ON vypujcky.id_ctenare = ctenari.id_ctenare
JOIN knihy ON vypujcky.id_knihy = knihy.id_knihy
WHERE ctenari.id_ctenare = 1;
```

---

### Závěr
Tento návrh rozšiřuje databázový model o tabulku čtenářů a výpůjček, čímž umožňuje evidenci výpůjček knih jednotlivými čtenáři.