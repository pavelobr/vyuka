# SQL a základní pojmy

## Databáze

Databáze je určitým způsobem organizovaná struktura informací uložená v souboru nebo ve skupině soborů uložených v nějakém úložišti. Úložištěm může být váš počítač u kterého právě sedíte nebo to může být úložiště někde na vzdáleném úložišti (cloud, počítač ve vaší počítačové síti, atd.).

## Relační databáze

Relační databáze jsou databáze, kde data jsou uložena v tabulkách. Zde jseou například data o lidech uložené v tabulce s názvem  *lidi*.

| id | jméno | příjmení | váha |
|---|---|---|---|
| 1 | Pavel | Novák | 62 |
| 2 | Eva | Veselá | 78 |
| 3 | Dan | Konečný | 25 |

Autorem myšlenky relačních databází je matematik [Edgar Frank Codd (Wikipedia)](https://cs.wikipedia.org/wiki/Edgar_Frank_Codd).

V tomto předmětu budeme pracovat převážne s relačními databázemi. Je však na místě upozornit, že exitují i jiné přístupy než relační model databáze. Takovými přístupy jsou například tzv. NoSQL databáze jako MongoDB, CouchDB a další. Například úložiště konfiguračních informací operačního systému Windows (registry) je příkladem hierarchické databáze.

## SQL

SQL je zkratka pro Structured Query Language, což je dotazovací jazyk, který nám umožňuje komunikovat s relační databází. Pomocí SQL získáváme data z databáze nebo data editujeme.

Příklad dotazu:

```sql
SELECT * from lidi;
```

Výsledek dotazu:

```batch
| id   | jmeno | prijmeni | vaha |
+------+-------+----------+------+
|    1 | Pavel | Novák    |   62 |
|    2 | Eva   | Veselá   |   78 |
|    3 | Dan   | Konečný  |   25 |
```

## Databázový program

Databázový program je software, který pracuje s databázovými soubory. Často je vytvořen na základě architektury klient - server.
Obvykle to je tak, že klient zašle požadavek serverové části a server se požadavek snaží uskutečnit. O výsledku požadavku pak prostřednictvím klienta uživatele uvědomí. Server se stará o správu dat a klienta využívá uživatel pro práci s databází.
Existují však "lehčí" databáze, kdy není třeba velké server - klient řešení.

### Zástupci SQL databázových programů

* MariaDB, MySQL (klient - server)
* PostgreSQL (klient - server)
* MS SQL Server (klient - server)
* Oracle Database (klient - server)
* SQLite (to je příklad "lehkého" řešení)
* ...a další

## Cíl předmětu

Cílem práce v našem předmětu je naučit se základní konstrukce jazyka SQL. SQL budete potřebovat při vytváření aplikací, které budou potřebovat uchovávat data a s daty pracovat.
Když procházíte internetové stránky, jsou vám prezentována data, která jsou uložena v různých databázích a předmět Webové aplikace vás čeká v příštím ročníku.
