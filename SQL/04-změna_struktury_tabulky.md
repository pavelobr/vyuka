# Změna struktury tabulky

## ALTER TABLE

Určitě je nejlepší při návrhu databáze a jejich tabulek dopředu vše dobře rozmyslet a nezabývat se doplňováním, mazáním či pozměňováním sloupců v tabulkách a tabulku rovnou správně navrhnout. Jsou však situace, kdy prostě musíme zasáhnout do struktury tabulky. Pro takové účely je určen příkaz `ALTER TABLE`.

Pro procvičení použijte dump databáze pujcovna [04-pujcovna.sql](04-pujcovna.sql).

## Přidání sloupce do tabulky

Máme tabulku `vypujcky`. Struktura tybulky `vypujcky`:

```sql
MariaDB [pujcovna]> DESCRIBE vypujcky;
+-----------------+----------------------+------+-----+---------+-------+
| Field           | Type                 | Null | Key | Default | Extra |
+-----------------+----------------------+------+-----+---------+-------+
| id              | smallint(5) unsigned | YES  |     | NULL    |       |
| jmeno           | varchar(20)          | YES  |     | NULL    |       |
| prijmeni        | varchar(20)          | YES  |     | NULL    |       |
| predmet         | varchar(20)          | YES  |     | NULL    |       |
| cena_s_dph      | decimal(9,2)         | YES  |     | NULL    |       |
| datum_vypujceni | date                 | YES  |     | NULL    |       |
| datum_vraceni   | date                 | YES  |     | NULL    |       |
+-----------------+----------------------+------+-----+---------+-------+
```

Rozšíření tabulku o sloupec `umisteni`, který se bude nacházet za sloupcem `predmet`:

```sql
# Vložení sloupce "umisteni" s datovým typem VARCHAR(50)
ALTER TABLE vypujcky ADD COLUMN umisteni VARCHAR(50) AFTER predmet;
```

Pokud bychom použili stejný příkaz bez klauzule AFTER, vložil by se sloupec `umisteni` na konec tabulky.

```sql
# Sloupec "umisteni" bude umístěn jako poslední
ALTER TABLE vypujcky ADD COLUMN umisteni VARCHAR(50);
```

Kdybychom potřebovali, aby byl sloupec umístěn jako první sloupec tabulky, musíme použít klauzuli `FIRST`.

```sql
# Sloupec "umisteni" bude umístěn jako prvni
ALTER TABLE vypujcky ADD COLUMN umisteni VARCHAR(50) FIRST;
```

## Změna existujícího sloupce v tabulce

Změnou existujícího sloupce v tabulce myslíme změnu jeho názvu, datového typu, umístění nebo všeho současně.
Pro změny využijeme klauzuli `CHANGE`.

```sql
# Změna názvu sloupce "umisteni" na "misto".
# Všimněte si, že uvádíme i datový typ.
ALTER TABLE vypujcky CHANGE umisteni misto VARCHAR(20);
```

Můžeme určovat i umístění sloupce pomocí klauzuli `FIRST` a `AFTER`.

```sql
# Změna názvu sloupce "umisteni" na "misto".
# Sloupec přesuneme za sloupec "predmet".
ALTER TABLE vypujcky CHANGE umisteni misto VARCHAR(20) AFTER predmet;
```

Pokud chceme pouze modifikovat vlastnosti sloupce, bez změny názvu sloupce, použijeme klauzuli `MODIFY`.

```sql
# Změna datového typu sloupce "misto"
ALTER TABLE vypujcky MODIFY misto VARCHAR(50);
```

```sql
# Změna datového typu sloupce "misto" a jeho umístění za sloupec prijmeni.
ALTER TABLE vypujcky MODIFY misto VARCHAR(50) AFTER prijmeni;
```

Poznámka na závěr. Vlastně si můžeme vystačit pouze se příkazem `ALTER TABLE` ve spojení s klauzulí `CHANGE`.

## Smazání sloupce tabulky

Nevratná změna struktury je spojena vždy s klauzulí `DROP`. `DROP TABLE` a `DROP DATABASE`, které známe, odstraní jmenované objekty. Podobně lze tedy odstranit sloupce v tabulce.

```sql
# Odstranění sloupce "misto".
ALTER TABLE vypujcky DROP misto;
```