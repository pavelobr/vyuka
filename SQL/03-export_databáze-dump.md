# Export databáze - dump

## mysqldump

Často se vyskytují situace, kdy je nutné data z databáze z různých důvodů převést do formy, které usnadňuje následující činnosti:

- záloha dat
- přenos dat do jiného databázového systému

Jednou z nejsnadnějších možností je vyexportovat, respektive udělat tzv. *dump* databáze. Výsledkem dumpu je sql skript, který obsahuje reverzní kroky pro vytvoření databáze se stejnou strukturou a daty (přesněji obsahuje námi dané reverzní kroky).
Pro systém *MariaDB* je přímo určen řádkový (CLI) nástroj `mysqldump`.

```bat
# Vytvoření zálohy s názevem backup.sql z databáze moje_db.sql.
# Záloha bude uložena v aktuálním adresáři.
# K databázovému systému budu přihlášen jako uživatel root.
# Všimněte si, že v souboru skriptu nebude CREATE DATABASE
mysqldump.exe --user=root -p moje_db > backup.sql
```

Řádková nástroj mysqldump je velmi mocný a umožňuje využít množství přepínačů, které ovlivňují obsah výsledného skriptu.

```bat
# Vyvolání nápovědy a zjištění přepínačů
mysqldump.exe --help
```

```bat
# Záloha všech databází v systému:
mysqldump --user=root -p -A > all_databases.sql
```

```bat
# Záloha jen vybraných databází v systému:
mysqldump --user=root -p -B database_prvni database_dalsi > muj_backup.sql
```

```bat
# Záloha jedné databáze.
# Chci, aby ve skriptu byla klauzule CREATE DATABASE
mysqldump --user=root -p -B moje_database > backup.sql
```

## Import z dumpu databáze

Výsledkem dumpu je sql skript, který interpretuje příkazy uložené v textovém souboru sqldumpu. Pokud chcete tyto příkazy provést (např. naimportovat databázi), využijte příkaz `SOURCE` řádkového klienta *MariaDB*.
Před spuštěním skriptu jej vždy prozkoumejte, může totiž obsahovat i příkazy, které mohou ohrozit stav vašich existujících databází!
Zamyslete se nad tím, co by provedl příkaz `DROP DATABASE jmeno_existujici_dulezite_databaze`.
Všimněte si, že za příkazem `SOURCE` není středník `;`.

```sql
# Provede příkazy uložené v souboru backup.sql.
# Soubor backup.sql je uložen v aktuálním adresáři.
SOURCE backup.sql
```

```sql
# Spuštění sql skriptu s uvedením celé cesty k souboru obsahující skript.
SOURCE C:\cesta_k_souboru\backup.sql
```