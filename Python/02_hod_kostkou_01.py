"""
Tento program simuluje hod kostkou
"""
# Importuji modul pro náhodnost
import random

# Hážu kostkou
hod = random.randint(1,6)

# Vypíši na konzoli výsledek
print("Hodili jste hodnotu: ", hod)
