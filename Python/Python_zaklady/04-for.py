# Demonstrace cyklu for
# Cyklus for vypíše hodnotu indexu i v mezích 1 až 6
"""
for i in range(1, 6):
    print("Hodnota i je právě", i)
"""
# Program Minutka

from time import sleep

cas = int(input("Zadej požadovaný čas v sekundách: [ENTER]: "))

# range vytvoří seznam celých čísel
for i in range(0, cas):
    print("Zbývá", cas - i, "sekund.")
    sleep(1)

print("HOTOVO!!!")
