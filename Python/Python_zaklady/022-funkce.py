# Program pro výpočet vlastností obdélníka
# Takto se píšou komentáře. Máme dva způsoby. Ten co vidíte nebo ten o řádek níže.
"""
Program spočítá
povrch a obvod obdélníka
"""
# strana_a = 12  # Tohle python zavede jako int
strana_a = input("Vlož délku strany a: ") # Pozor bude to str (string), protože input vždy zavede str.
strana_a = float(strana_a) # Změna typu se provede pomocí float(), str(), int()
strana_b = 5.9 # Tohle python zavede jako float. Python má pouze float  a ne double (zjednodušení)
 
# obvod = 2 * (strana_a + strana_b)
 
# Funkce se definuje klíčovým slovem def a hlavička funkce končí dvojtečkou
# Funkce může mít parametry nebo nemusí, pokud je nemá tak je závorka prázdná
def obdelnik_obvod(a, b):
    ''' Spočítá obvod obdélníka '''
    o = 2 * (a + b)
    return o
 
obvod = obdelnik_obvod(strana_a, strana_b)
 
print("Obvod je", obvod)