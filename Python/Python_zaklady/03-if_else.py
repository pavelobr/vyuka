# Hra kámen, nůžky, papír
# Demostrace použití if a else

import random

print("########################################")
print("#                                      #")
print("#       kámen, nůžky, papír            #")
print("#                                      #")
print("########################################")
print()

volba_pocitac = random.choice(["kámen", "nůžky", "papír"])

volba_hrac = input("kámen nůžky nebo papír? [ENTER] ")


if volba_pocitac == volba_hrac:
    print("Remíza!")
elif volba_pocitac == "kámen" and volba_hrac == "nůžky":
    print("Počítač vyhrál. Počítač zvolil", volba_pocitac)
elif volba_pocitac == "kámen" and volba_hrac == "papír":
    print("Počítač prohrál. Počítač zvolil", volba_pocitac)
elif volba_pocitac == "nůžky" and volba_hrac == "papír":
    print("Počítač vyhrál. Počítač zvolil", volba_pocitac)
elif volba_pocitac == "nůžky" and volba_hrac == "kámen":
    print("Počítač prohrál. Počítač zvolil", volba_pocitac)
elif volba_pocitac == "papír" and volba_hrac == "kámen":
    print("Počítač vyhrál. Počítač zvolil", volba_pocitac)
elif volba_pocitac == "papír" and volba_hrac == "nůžky":
    print("Počítač prohrál. Počítač zvolil", volba_pocitac)
else:
    print("Pořádně napiš papír, kámen, nebo nůžky!")
