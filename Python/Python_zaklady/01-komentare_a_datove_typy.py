##################################################
# Komentáře, základní datové typy a funkce print #
##################################################

# Tohle je komentář
# Toto také
""" Komentář¨
na více řádků """
''' Tohle je také komentář
na více řádků '''

## Tisk na konzoli (funkce print)

print("Ahoj lidi!") # Tady můžete komentovat
print('Ahoj lidi!')

print('Hanka řekla: "Těším se na písemnku!".')   # Tohle funguje
print("Hanka řekla: \"Těším se na písemnku!\".") # Tohle funguje

## Proměnné a datové typy

jmeno = "Pavel"  # string datový typ
prijmeni = "Obr" # string datový typ

print(jmeno, prijmeni) # Vytiskne Pavel Obr

strana_a = 5    # integer datový typ
strana_b = 5.6  # float datový typ

obsah_obdelnika = strana_a * strana_b
type(obsah_obdelnika) # zjisti datový typ (Jaký datový typ má proměnná obsah_obdelnika?)

print("Obsah obdélníka je", obsah_obdelnika, "cm2")

# Kdo je učitel?
ucitel = True # boolean datový typ
zak = False   # boolean datový typ

## Změna datového typu

print(type(obsah_obdelnika))
obsah_obdelnika = str(obsah_obdelnika) # str() změna datového typu na string, pokud bych chtěl změnit na float nebo integer použiju float() nebo int()
print(type(obsah_obdelnika))

## Něco navíck k print

print(jmeno + prijmeni)
print(jmeno + " "  + prijmeni)
print(3 * jmeno)
print(3 * (jmeno + " "))

## Úkoly
# Vytiskněte na konzoli:

#########################
##                     ##
##   Webové aplikace   ##
##                     ##
#########################