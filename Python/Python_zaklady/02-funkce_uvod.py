######################################
#     Definice jednoduché funkce     #
######################################

# Jak získat vstup (STDIN) od uživatele na konzoli (STDOUT) pomocí funkce input()

jmeno_uzivatele = input("Zadej své křestní jméno: [ENTER] ") # vzdy bude vysledkem datový typ string str()
prijmeni_uzivatele = input("Zadej své příjmení: [ENTER] ")

## Definice funkce - klíčové slovo def
# Příklad funkce bez parametru
def inicialy_obra():
    """ Funkce vytiskne iniciály Pavla Obra. """
    jmeno = "Pavel"
    prijmeni = "Obr"
    print("Tvé iniciály jsou", jmeno[0] + "." + prijmeni[0] + ".")

# Zavolání (provedení) funkce
inicialy_obra()


# Funkce s parametry
def inicialy_uzivatele(jmeno, prijmeni):
    """ Funkce vytiskne iniciály z předaného jmena a prijmeni. """
    print("Tvé iniciály jsou", jmeno[0] + "." + prijmeni[0] + ".")

# Zavolání funkce s předanými parametry
inicialy_uzivatele(jmeno_uzivatele, prijmeni_uzivatele)



