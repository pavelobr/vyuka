# Demonstrace cyklu while
# Program minutka

from time import sleep

cas = int(input("Zadej požadovaný čas v sekundách: [ENTER]: "))

# Dokud proměnná cas nebude rovna 0 bude se cyklus vykonávat
while cas != 0:
    sleep(1)
    cas = cas - 1
    print("Zbývá", cas, "sekund.")

print("HOTOVO!!!")
