#############################################################
# Jak získat vstup (STDIN) od uživatele na konzoli (STDOUT) #
#############################################################

jmeno_uzivatele = input("Zadej své křestní jméno: [ENTER] ") # POZOR! Vždy bude výsledkem datový typ string str()
prijmeni_uzivatele = input("Zadej své příjmení: [ENTER] ")

print("Tvoje celé jméno je", jmeno_uzivatele, prijmeni_uzivatele + ".")
# Mohu vypsat iniciály, vzpomeňte si na index. 0 je první písmeno, 1 druhé ... 
print("Tvé iniciály jsou", jmeno_uzivatele[0] + "." + prijmeni_uzivatele[0] + ".")

vek_uzivatele = int(input("Kolik Ti je let? [ENTER]")) #  mohu prevést na jiný datový typ, v tomto případě int()
do_stovky = 100 - vek_uzivatele # kdybych nepřevedl vek_uzivatele na int() skončilo by to chybou

print("Do stovky Ti zbývá", do_stovky, " let.")




