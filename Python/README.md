# Skripty určené pro seznámení s Pythonem

Při výuce vycházíme z toho, že máte znalosti o běžných konstrukcích programovacího jazyka.
Již druhý rok se učíte programovat v jazyce C++, takže mnoho konstrukcí používate v jazyce C++.
Jde nám hlavně seznámení se se syntaxí a filozofií jazyka Python. Všechny zde publikované příklady
využívají Python verze 3.x.

## Materiály k výuce

### W3schools

Na https://www.w3schools.com/python/default.asp je pěkný interaktivní kurz Pythonu, kde jsou předvedeny všechny základní vlastnosti Pythonu. Doporučuji!

### Tutorialspoint

Také na https://www.tutorialspoint.com/python3/ je k dispozici přehledný průvodce základy Pythonu.

### Z českých luhů a hájů

Tak jako i v jiných oblastech IT jsou materiály v češtině dostupné v menším množství, přesto existuje dost kvalitních materiálů pro začátečníky:

* Sallyx.org - https://www.sallyx.org/sally/python/
* Python.cz - https://naucse.python.cz/course/pyladies/
* Kniha "Ponořme se do Pythonu 3"
  * Webová verze: http://diveintopython3.py.cz/index.html
  * Kniha ke stažení: https://www.root.cz/knihy/ponorme-se-do-pythonu-3/

## Základní datové typy, proměnné, import modulu

Soubory s maskou:

```bash
01*.py
```

V Pythonu nemusíme předem deklarovat proměnnou, ale můžeme ji hned zavést.

Pro proměnné máme různé datové typy. Zde použijeme string (str), integer (int) a float pro čísla s desetinnou čárkou.
Python rozeznává i jiné datové typy, ty potkáme později.

V C++ máme knihovny, v pythonu máme tzv. moduly, které importujeme.

## Větvení kódu (if, elif, else), cyklus while

Soubory s maskou:

```bash
02*.py
```

V Pythonu si musíte zvyknout na to, že odsazení (4 mezery/tab) znamená blok kódu, který se bude
vykonávat v těle cyklu, podmínky, apod..

```python
# Toto funguje, všimněte si, že kód, který se má vykonávat
# v těle podmínky je odsazen.
if 5 > 4:
    print("Tohle je pravda a skript bude fungovat")

# Protože není kód v podmínce if odsazen, nebude to fungovat
# a navíc skript skončí chybou:
if 5 > 4:
print("Tohle je pravda a skript nebude fungovat")
```

V Pythonu máme cyklu typu ```for``` a ```while```. V této kapitole ukazujeme syntaxi cyklu typu ```while```.

## Cykly

Python má pouze dva typy cyklů - ```for``` a ```while```. Příklady, jak se cyklus v Pythonu tvoří jsou v souborech s maskou:

```bash
03*.py
```

## Výjimky (odchytávání výjimek)

Pokud dojde během vykonávání skriptu k chybě, vyvolá se tzv. výjimka. Například,
pokud dojde k dělení nulou, vyvolá se výjimka ```ZeroDivsionError```.

```python
>>> cislo = 12 / 0
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
ZeroDivisionError: division by zero
```

Výjimek je mnoho typů, ale výhoda je, že se toho dá využít při tvorbě skriptu. Příklady využití odchytávání výjimek naleznete v souborech s maskou ```04*.py```.

## Seznamy, uspořádané n-tice a slovníky

Python disponuje množstvím datových struktur. Pro naší výuku jsou zásadní tyto typy:

* Seznamy
* Uspořádané n-tice
* Slovníky

### Seznam
