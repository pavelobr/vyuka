"""
Tento program simuluje hod kostkou
"""
# Importuji jen to, co potřebuji:
from random import randrange

# Toto mi umožní pozastavit běh programu na určitý časový interval
from time import sleep

odpoved = input("Chceš pokračovat? [Ano/Ne]: ")

# Příklad cyklu typu while
while odpoved == "Ano":
    # Hážu kostkou
    hod = randrange(1,7,1)

    # Vypíši na konzoli výsledek
    print("Hodili jste hodnotu: ", hod)

    # Příklad větvení kódu (podmínka)
    if hod > 3:
        print("Hodili číslo větší než 3.")
    elif hod == 3:
        print("Hodili jste číslo rovné 3.")
    else:
        print("Hodili jste číslo menší než 3.")
    # Čeká daný počet sekund
    sleep(1)
    odpoved = input("Chceš pokračovat? [Ano/Ne]: ")
