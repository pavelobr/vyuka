"""
Tento program simuluje minutku - například na troubě.
"""

from time import sleep

cas = input("Zadej čas v sekundách [Enter]: ")
cas = int(cas)  # Převádíme string na integer

# Cyklus pro odpočítávání pomocí cyklu while.
# Dokud nebude cas = 0, bude se cyklus provádět.
while cas != 0:
    print("Zbývá", cas, " sekund.")
    sleep(1)  # Čeká 1 sekundu
    cas = cas - 1  # Čas se sníží o 1 sekundu.

print("Konec odpočítávání!")
