# -*- coding: utf-8 -*-
# Toto je komentář pro jeden řádek.
# Dbejte, aby soubor měl kódování UTF-8!
"""
(Takto můžme komentovat přes více řádků.)
V Pythonu máme tzv. moduly (knihovny). Můžeme je připojit tak,
že importujeme VŠECHNO z modulu:
"""
import math

# Nebo můžeme importovat jen to, co chceme:
from datetime import date

# Takto tiskneme do konzole:
print("####################################")
print("# Tento skript spočítá objem válce #")
print("####################################")

# Získání vstupu od uživatele z konsole. Pozor! Výsledný datový typ je str (string).:
r = input("Zadej poloměr válce v m [Enter]: ")
h = input("Zadej výšku válce v m [Enter]: ")

# Aby bylo možné provádět výpočet, musíme string (str) převést na číslo (float)
r = float(r)
h = float(h)

# Konečně výpočet (z modulu math je math.pi je Pi - 3.14 math.pow je umocňování).
# Všimněte si, že uvádím před vybranou funkcí název importovaného modulu.
# Důvodem je použitý způsob importu. Viz: import math
v = math.pi * math.pow(r, 2) * h

# Dnešní datum získávám z modulu datetime. Všimněte si, že neuvádím před používanou
# funkcí název importovaného modulu. Důvodem je použitý způsob importu. Viz: from datetime import ...
dnes = date.today()

print("Dnes je", dnes, ".")
print("Objem válce je ", v, "m3.")
