"""
Tento program simuluje minutku - například na troubě.
"""

from time import sleep

cas = input("Zadej čas v sekundách [Enter]: ")
cas = int(cas)  # Převádíme string na integer

# Cyklus for.
# Funkce range vytvoří řadu čísel.
# Pokud je čas = 5. Tak je řada tvořena: 5,4,3,2,1,0.
# Proto je krok pro procházení řady -1.
for i in range(cas, 0, -1):
    print("Zbývá", i, " sekund.")
    sleep(1)  # Čeká 1 sekundu

print("Konec odpočítávání!")
