# Informace

V tomto repozitáři naleznete jsou zdrojové kódy, které požíváme při výuce.
Jsou tu materiály pro:

* Python
* Flask (píšeme web)
* SQL (základní práce s MariaDB/MySQL)
