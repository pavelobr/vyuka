from flask import Flask
from random import choice

app = Flask(__name__)
app.debug = True

prislovi_db = [
    "Kdo jinému jámu kopá, sám do ní padá",
    "Jak se do lesa volá, tak se z lesa ozývá.",
    "Jablko nepadá daleko od stromu.",
    "Tak dloho se chodí se džbánem pro vodu, až se ucho utrhne.",
    "Nesuď dne před večerem.",
    "Ráno moudřejší večera.",
    "Dvakrát měř, jednou řež.",
    "Co můžeš udělat dnes, neodkládej na zítřek.",
]


@app.route('/')
def prislovi_dne():
    prislovi = choice(prislovi_db)
    return prislovi


if __name__ == '__main__':
    app.run()
