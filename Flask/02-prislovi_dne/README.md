# Přísloví dne

Možná se to nezdá, ale už umíme dost, abychom vytvořili první webovou aplikaci, která je skutečně dynamická. Naše aplikace bude návštěvníkovi zobrazovat přísloví. Přísloví bude vybíráno náhodně ze seznamu různých přísloví. Je tedy určitá pravděpodobnost, že by při dostatečně velkém seznamu různých přísloví by měl uživatel vidět pokaždé přísloví jiné.

## Příprava

Založíme si virtualenv, aktivujeme jej a nainstalujeme flask:

```bat
python -m venv H:\Devel\prislovi_dne\venv
cd H:\Devel\prislovi_dne
venv\Scripts\activate.bat
pip install flask
```

## Náhodný výběr přísloví

V souboru `prislovi.py` je skript, který zobrazí vždy jedno přísloví. Výběr náhodného přísloví je zajištěn použítím funkce `choice` z modulu `random`, který generuje tzv. pseunáhodná čísla.

## Vytvoření webové aplikace

Logika losování náhodného přísloví je zřejmá ve skriptu `prislovi.py`.
Vezměme si z předchozí úlohy `01-hello_world` soubor `app.py`, který použijeme jako šablonu. Do naší šablony vložíme vše potřebné ze skriptu `prislovi.py`:

* Naimportujeme z modulu `random` funkci `choice`.
* Zkopírujeme databázi přísloví `prislovi_db`.
* Upravíme funkci `hello_world()` na `prislovi_dne()` a funkci pozměníme tak, aby vybírala náhodně přísloví `choice(prislovi_db)`.
* Funkce `prislovi_dne()` bude vracet náhodné přísloví `return prislovi`.

## Spuštění webové aplikace

Spustíme webovou aplikaci: `python app.py`. Na adrese http://127.0.0.1:5000/ bychom měli vidět naší aplikaci v provozu. Zkuste znovu načíst webovou stránku *(Ctrl + R nebo F5)*, mělo by se znovu vylosovat a zobrazit jiné přísloví.

Pokud budeme aplikaci publikovat veřejně, nezapomeňte nastavit `app.debug = False`.
