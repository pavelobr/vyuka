# Aplikace on-line poznámky

## Co jsme už vytvořili 
Aplikace umí:

1. Obsahuje formulář pro vložení poznámky
2. Obsahuje výpis všech poznámek
3. Má Váš pěkný vzhled (ta má ji nemá:)).
4. Umí smazat poznámku

Co ještě aplikace musí umět:

4. Upravit existující poznámku.
5. Změňte aplikaci tak, aby se dala zapisovat důležitost poznámky (Vysoká, nízká,...počet stupňů důležitosti 
nechám na vás). Musíte upravit databázi (přidat sloupec) a i formulář.
6. Zatím nebude chráněná žádnými přihlašovacími údaji - každý bude moci něco vložit

## Úkoly do příštího týdne

1. Zařiďte, aby bylo možné poznámky upravovat. Spusťte si zde publikovanou aplikaci, je to zde vyřešené.
Prozkoumejte kód a začleňte editaci poznámek do vaší aplikace. V kódu jsou vysvětlující komentáře, přečtěte si je!
Všimněte si, jak jsem definoval cestu k databázi (budete to potřebovat pro pythonanywhere.com). Podívejte, jak jsou
vytvářené dotazy do databáze a podívejte se na odkazovanou zmínku o SQL injection útoku.
2. Až vám bude fungovat vaše aplikace s funkčností popsanou v bodě 1 pokračujte dále a změňte aplikaci tak, aby se dala zapisovat důležitost poznámky (Vysoká, nízká,...počet stupňů důležitosti 
nechám na vás). Musíte upravit databázi (přidat sloupec) a i formulář. Zda bude důležitost vyjádřena textem, barvou, zvýrazněním už nechám
na vás.
3. Publikujte aplikaci na https://pythonanywhere.com.
4. Zjistěte co je Git (https://www.itnetwork.cz/software/git/git-tutorial-historie-a-principy)
5. Stáhněte si aplikaci GitHub Desktop (https://desktop.github.com/) (pokud nebude tento nástroj potřebovat, nedělejte to)
6. Založte si repozitář a umístěte kód aplikace na github. Pošlete mi odkaz, kde najdu kód vaší aplikace.
(Termín je do úterý 21.4.)

Poznámka na závěr: Pokud někdo chce použít jinou službu stejného určení než github, použijte tu vaší vybranou.
