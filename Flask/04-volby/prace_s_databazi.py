import sqlite3

# Připojení k databázi
conn = sqlite3.connect('volby.db')

# vystaíme objekt c, který realizuje operace v databázi
c = conn.cursor()

# Vytvoření tabulky a vložení dat do tabulky
c.execute('CREATE TABLE jmena(id INT, jmeno CHAR(20), prijmeni CHAR(20))')
c.execute('INSERT INTO jmena (id, jmeno, prijmeni) VALUES (1, "Pavel", "Obr")')
c.execute('INSERT INTO jmena (id, jmeno, prijmeni) VALUES (2, "Zdenek", "Kulhánek")')

# Tisk obsahu tabulky jmena
for row in c.execute('SELECT * FROM jmena'):
    print(row)
