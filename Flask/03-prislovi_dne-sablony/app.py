# importujeme další modul, ten nám umožňuje pracovat
# se šablonami
from flask import Flask, render_template
from random import choice

app = Flask(__name__)
app.debug = True

prislovi_db = [
    "Kdo jinému jámu kopá, sám do ní padá",
    "Jak se do lesa volá, tak se z lesa ozývá.",
    "Jablko nepadá daleko od stromu.",
    "Tak dloho se chodí se džbánem pro vodu, až se ucho utrhne.",
    "Nesuď dne před večerem.",
    "Ráno moudřejší večera.",
    "Dvakrát měř, jednou řež.",
    "Co můžeš udělat dnes, neodkládej na zítřek.",
]


@app.route('/')
def prislovi_dne():
    prislovi = choice(prislovi_db)
    # Budu vykreslovat šablonu 'prislovi.html' v adresáři 'static/css'.
    # Šablonu budu naplňovat hodnotou proměnné 'prislovi'.
    # Hodnota 'prislovi' bude umístěna do 'text'.
    return render_template('prislovi.html', text=prislovi)


if __name__ == '__main__':
    app.run()
