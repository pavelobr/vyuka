"""
Program náhodně vybírá ze seznamu přísloví jedno přísloví
a zobrazí ho uživateli.
"""

from random import choice

# Seznam přísloví máme uložený v datové struktuře seznam.
# Co je seznam je popsáno
# například tady: https://www.sallyx.org/sally/python/python3b.php#list
# nebo tady: https://www.w3schools.com/python/python_lists.asp

prislovi_db = [
    "Kdo jinému jámu kopá, sám do ní padá",
    "Jak se do lesa volá, tak se z lesa ozývá.",
    "Jablko nepadá daleko od stromu.",
    "Tak dloho se chodí se džbánem pro vodu, až se ucho utrhne.",
    "Nesuď dne před večerem.",
    "Ráno moudřejší večera.",
    "Dvakrát měř, jednou řež.",
    "Co můžeš udělat dnes, neodkládej na zítřek.",
]

prislovi = choice(prislovi_db)

print(prislovi)
