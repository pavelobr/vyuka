from flask import Flask

app = Flask(__name__)
# Zapnutí ladění
app.debug = True


@app.route('/')
def hello_world():
    return "Ahoj světe!"


if __name__ == '__main__':
    app.run()

