# První webová aplikace

Naše první webová aplikace nedělá nic užasného, ale naučí nás spouštět vývojový webový server a poskytne nám šablonu pro vývoj našich budoucích skvělých aplikací.

## Jak spustit

Předpokládejme, že aplikaci spouštíme skriptem `app.py`.
Abychom spustili vyvíjenou aplikaci, dodržme pro snadnost tento postup:

* Jsme v adresáři, kde je umístěn soubor `app.py`.
* Pokud nemáme aktivovaný `virtualenv`, tak jej aktivujme.
* Aplikaci spustíme příkazem `python app.py`.

Pokud je vše jak má být, měli bychom se dočkat následujícího výstupu z konsole:

```dos
 * Serving Flask app "app" (lazy loading)
 * Environment: production
   WARNING: Do not use the development server in a production environment.
   Use a production WSGI server instead.
 * Debug mode: off
 * Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)
```

Důležitou informací je pro nás to, že v internetovém prohlížeči na adrese http://127.0.0.1:5000/ čeká naše první aplikace!
Pokud v internetovém prohlížeči vydíte pozdrav `Ahoj světe!`, vytvořili jste první webovou aplikaci s využitím frameworku `flask`.

## Jak povolit debugging

Debugging je výraz pro tzv. ladění programu. Vývojový server lze spustit se zapnutým debuggingem (laděním). Tento režim používáme pouze pro vývoj! Zapnutí ladících informací nám umožní najít a opravit chyby, které budeme nevyhnutelně vytvářet. Dále jako bonus nám vývojový server při změně v kódu aplikaci restartuje.

Jakmile aplikaci spustíme veřejně, je nutné debugging vypnout. Pokud to neuděláte, připravte se na napadení vaší aplikace nezvaným návštěvníkem z Internetu.

Debugging zapnete tak, že nastavíte ve skriptu `app.py` `app.debug` na `True`:

```python
app.debug = True
```

Pokud poté znovu spustíme webovou aplikaci, tak bychom měli získat z konsole tento výstup:

```dos
 * Serving Flask app "app" (lazy loading)
 * Environment: production
   WARNING: Do not use the development server in a production environment.
   Use a production WSGI server instead.
 * Debug mode: on
 * Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)
 * Restarting with stat
 * Debugger is active!
 * Debugger PIN: 130-115-276
```

Všimněte si hlavně řádku s informací `Debugger is active!`. Nezapomeňte debugging vypnout, až budete aplikaci zveřejňovat.