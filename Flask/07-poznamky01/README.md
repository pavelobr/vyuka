# Aplikace on-line poznámky

## Co jsme už vytvořili 
Jak bude aplikace vypadat?

1. Bude obsahovat formulář pro vložení poznámky
2. Bude obsahovat výpis všech poznámek
3. Zatím nebude chráněná žádnými přihlašovacími údaji - každý bude moci něco vložit

## Úkoly do příštího týdne

1. Zařiďte, aby bylo možné poznámky mazat. Spusťte si zde publikovanou aplikaci, je to zde vyřešené.
Prozkoumejte kód a začleňte mazání poznámek do vaší aplikace. V kódu jsou vysvětlující komentáře, přečtěte si je!
Všimněte si, jak jsem definoval cestu k databázi (budete to potřebovat pro pythonanywhere.com). Podívejte, jak jsou
vytvářené dotazy do databáze a podívejte se na odkazovanou zmínku o SQL injection útoku. Bude vás určitě
zajímat to, jak jsem změnil velikost textarea ve formluáři (viz. css).
2. Zařiďte, aby poznámky byly řazeny tak, že první v pořadí bude vždy poslední vložená poznámka (podle data a času).
3. Změňte aplikaci tak, aby na úvodní straně (/) byl seznam poznámek a na URL(/poznamka/vlozit) byl formluář pro vkládání poznámek.
4. Bude hezká:). Pokuste se vytvořit vzhled, kde bude poznámka jako lístek připíchnutý na nástěnku,  případně
položený papírek na stole. Nechám to na vás. Určitě bude dobré zapomenout na tabulku a podívat se sem: 
https://www.w3schools.com/css/css3_flexbox.asp, https://www.w3schools.com/cssref/css_units.asp
5. Publikujte aplikaci na https://pythonanywhere.com.
6. Zašlete mi kód aplikace (do cloudu) a odkaz na běžící aplikaci. Termín je do čtvrtka 2.4. :)
