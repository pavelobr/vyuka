from flask import Flask, session, render_template, redirect
from flask_wtf import FlaskForm
from wtforms import TextField, TextAreaField, SubmitField
from wtforms.validators import DataRequired

import sqlite3
import os

app = Flask(__name__)

app.secret_key = 'dgbdbdfdfb dfbndbn'
# Nezapomeňte při vystavení na internetu přepnout na False!
app.debug = True

# Slouží pro nastavení správné cesty k databázi
aktualni_adresar = os.path.abspath(os.path.dirname(__file__))

class NastavPrezdivkuForm(FlaskForm):
    """ Vystavuje formulář pro nastavení přezdívky. """
    prezdivka = TextField("Přezdívka")
    odeslat = SubmitField("Přihlásit")


class ZpravaForm(FlaskForm):
    """ Vystavuje formulář pro vkládání nové zprávy. """
    obsah = TextAreaField("Zpráva", validators=[DataRequired()]) # Je vyžadováno vyplnění
    odeslat = SubmitField("Odeslat")


@app.route('/', methods=['GET', 'POST'])
def index():
    """ Řeší přihlášení uživatele chatu - nastavení přezdívky."""
    form = NastavPrezdivkuForm()
    if form.validate_on_submit():
        # Nastavuje prezdivku uložením do relace
        session['prezdivka'] = form.prezdivka.data
        return render_template('prihlasit.html', form=form, prezdivka=session['prezdivka'])
    try:
        return render_template('prihlasit.html', prezdivka=session['prezdivka'], form=form)
    except:
        return render_template('prihlasit.html', form=form)


@app.route('/chat', methods=['GET', 'POST'])
def vloz_zpravu():
    """ Zobrazuje zprávy a vkládá nové zprávy. """
    # connection = sqlite3.connect('chat.db')
    connection = sqlite3.connect(os.path.join(aktualni_adresar, 'chat.db'))
    # https://docs.python.org/3/library/sqlite3.html#sqlite3.Row
    connection.row_factory = sqlite3.Row
    cursor = connection.cursor()
    # Výběr všech dat v databázi
    cursor.execute('SELECT * FROM zpravy ORDER BY id DESC')
    # Naplní proměnnou zpravy (jedná se o pole) výsledkem předchozího dotazu
    zpravy = cursor.fetchall()
    form = ZpravaForm()
    if form.validate_on_submit():
        # Naplní proměnnou obsah daty z formuláře
        obsah = form.obsah.data
        # Jak vytvářet SQL dotazy:
        # http://bobby-tables.com/python
        # https://docs.python.org/3/library/sqlite3.html
        cursor.execute("INSERT INTO zpravy(prezdivka,obsah) VALUES(?,?)", (session['prezdivka'], obsah,))
        # Vložení dat do databáze
        connection.commit()
        # Přesměruje zpět na formulář
        return redirect("/chat")
    # Uzavře připojení k databázi
    connection.close()
    try:
        return render_template('chat.html', prezdivka=session['prezdivka'],
                               zpravy=zpravy, form=form)
    except:
        # Když nemá uživatel přezdívku, je přesměrován na přihlášení
        return redirect("/")



@app.route('/zpravy')
def zpravy():
    """Zobrazí všechny zprávy z databáze."""
    # connection = sqlite3.connect('chat.db')
    connection = sqlite3.connect(os.path.join(aktualni_adresar, 'chat.db'))
    # https://docs.python.org/3/library/sqlite3.html#sqlite3.Row
    connection.row_factory = sqlite3.Row
    cursor = connection.cursor()
    # Výběr všech dat v databázi
    cursor.execute('SELECT * FROM zpravy ORDER BY id DESC')
    # Naplní proměnnou zpravy (jedná se o pole) výsledkem předchozího dotazu
    zpravy = cursor.fetchall()
    return render_template('zpravy.html', zpravy=zpravy)


@app.route('/odhlasit')
def odhlasit():
    # session je jako pythonovský slovník. Metoda pop vymaže klíč s hodnotou.
    session.pop('prezdivka')
    # Přesměruje uživatele na přihlášení (je vyžadována přezdívka)
    return redirect('/')


if __name__ == '__main__':
    app.run()