# Flask

Flask je jeden z mnoha frameworků pro vytváření dynamických webových aplikací. Je vhodný jako výukový nástroj, ale pohání i významné služby na Internetu (např. Pinterest).

## Virtualenv

### Vytvoření prostředí pro vývoj

Pro vývoj je vhodné si vytvořit oddělené prostředí. V takovém prostředí poté můžeme mít vybrané moduly, které použijeme pouze v aktuálním projektu. Díky tomu budeme mít kontrolu nad závislostmi. Virtuální prostředí se označuje jako `virtualenv`.

Tento příkaz vytvoří virtuální prostředí s názvem `venv` v adresáři `H:\Devel\hello_world\venv`:

```dos
python -m venv H:\Devel\hello_world\venv
```

### Aktivace a deaktivace virtualenv

Pracovat ve virtálním prostředí lze po jeho aktivaci. Příslušný `virtualenv` aktivujeme takto:

```dos
H:\Devel\hello_world\venv\Scripts\activate.bat
```

Pokud chceme práci ve virtualenv ukončit, napíšeme příkaz `deactivate`:

```dos
deactivate
```

## Instalace modulů

Pokud chceme používat některý z modulů, které nemáme v naší instalaci Pythonu k dispozici, můžeme
je snadno doinstalovat z https://pypi.org/. Instalace probíhá pohodlně prostřednictvím příkazu `pip`,
který je součástí standartní instalace Pythonu ve Windows.



Například takto nainstalujeme `flask` (nezapomeňte - v aktivovaném virtualenv):

```dos
pip install flask
```

> Nezapomeňte nejprve vytvořit a aktivovat příslušný virtualenv. Věřte, je to rozumné.