# Aplikace on-line poznámky

## Požadavky na aplikaci 
Jak bude aplikace vypadat?

1. Bude obsahovat formulář pro vložení poznámky
2. Bude obsahovat výpis všech poznámek
3. Zatím nebude chráněná žádnými přihlašovacími údaji - každý bude moci něco vložit
4. Bude hezká

## Výchozí aplikace
Jak na to?

Zpočátku to bude velmi jednoduché. Vezměte naší poslední aplikaci (anketu na internetu) a jen ji lehce modifikujme.
Kód této aplikace je uložen v adresáři, kde se právě nacházíte (naklonujte si celý tento adresář nebo stáhněte soubor kod_aplikace.zip). Kód obsahuje některé úpravy, které se vám budou
hodit. Tou zásadní je zobrazení tabulky s výsledky hlasování.

Založte si prostředí pro vývoj. Viz. známé informace, které jsou uvedené v README nadřazené kapitoly.

Nezapomeňte nainstalovat všechny potřebné moduly:

* flask
* flask-wtf

Aplikaci pak stačí jen spustit. Prozkoumejte její funkci, pak její zdrojový kód a zaměřte se komentáře.

## Modifikace aplikace

### Úkoly do 25.3. 2020
Zásadní modifikace aplikace bude spočívat v tomto:

* Změňte formulář AnketaForm:

```python
# V importech přidejte:
from wtforms import TextAreaField

# třídu AnketaForm odstraňte a nahraďte ji formulářem pro text poznámky
class PoznamkaForm(FlaskForm):
    poznamka = TextAreaField("Poznámka", validators=[DataRequired()])
```
* Zajistěte, aby poznámka mohla mít maximálne 250 znaků. Poznámka: Podívejte se do dokumentace wtforms (https://wtforms.readthedocs.io/en/stable/).

* Budete potřebovat jinou databázi, která bude obsahovat dva sloupce - poznamka (text poznámka) a vlozeno (datum vložení)


* Upravte zbylý kód tak, aby vše fungovalo jak má. Pokud to je nutné, tak opravte názvy proměnných tak, aby odpovídaly zadání.

* Seznam poznámek bude seřazen tak, aby první v řadě byla poslední vložená poznámka.
