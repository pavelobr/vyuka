import sqlite3
from flask import Flask, render_template, redirect
from flask_wtf import FlaskForm
from wtforms import RadioField
from wtforms.validators import DataRequired

app = Flask(__name__)
app.debug = True
app.secret_key = 'svsdfvsdfvbadb  sfgsgsrg'


class AnketaForm(FlaskForm):
    hlas = RadioField('čemu dáváte přednost', choices=[('voda', 'Voda'),
                                                       ('vino', 'Víno'),
                                                       ('limo', 'Limonáda')])

@app.route('/', methods=['GET', 'POST'])
def hlasuj():
    """Zobrazí hlasovací formulář."""
    form = AnketaForm()
    uzivatel_hlas = form.hlas.data
    if form.validate_on_submit():
        conn = sqlite3.connect('anketa.db')
        c = conn.cursor()
        c.execute(f"INSERT INTO anketa({uzivatel_hlas}) VALUES (1)")
        conn.commit()
        conn.close()
        return redirect('/vysledky')
    return render_template('hlasuj.html', form=form)


@app.route('/vysledky')
def zobraz_vysledky():
    """Zobrazí výsledky hlasování."""
    conn = sqlite3.connect('anketa.db')
    c = conn.cursor()
    c.execute("SELECT SUM(voda), SUM(vino), SUM(limo) FROM anketa")
    vysledky = c.fetchone()
    conn.close()
    return render_template('vysledky.html', vysledky=vysledky)


if __name__ == '__main__':
    app.run()
